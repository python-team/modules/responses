requests<3.0,>=2.30.0
urllib3<3.0,>=1.25.10
pyyaml

[tests]
pytest>=7.0.0
coverage>=6.0.0
pytest-cov
pytest-asyncio
pytest-httpserver
flake8
types-PyYAML
types-requests
mypy
tomli-w

[tests:python_version < "3.11"]
tomli
